<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<title>Jurodca - Java Developer Test - CIAT</title>
	
</head>
<body >
	<div class="container">
		<s:if test="hasActionMessages()">
		   <div>
		      <s:actionmessage />
		   </div>
		</s:if>
		<s:if test="hasActionErrors()">
		   <div>
		      <s:actionerror/>
		   </div>
		</s:if>
	     
	    <h2>Request a new institution or branch</h2>
	    
	    <p>
	    	Please enter the institution as "Partner", the legal entity.<br>
	    	The name of the institution should be in its official language. (e.g. for CIAT: Centro Internacional de Agricultura Tropical). 
	    </p>
	    
	    <s:form id="frmPartners" method="post" action="addPartner">
	    	<div >
	    		<s:label>Is this institution a branch? </s:label>
	    		<s:submit id="idYes" value="Yes"/>
	    		<s:submit id="idNo" value="No"/>
	    	</div>	
	    	
	    	<div>
	    		<s:textfield name="partner.headquarter" label="Select institution headquarter" />
	    		<s:textfield name="partner.acronym" label="Acronym" />
	    		<s:textfield name="partner.name" label="Name"/>
	    		<s:textfield name="partner.type" label="Type"/>
	    		<s:textfield name="partner.country" label="Country"/>
	    		<s:textfield name="partner.city" label="City"/>
	    		<s:textfield name="partner.website" label="If you know the partner website please paste the link above"/>
	    		
	    		<s:submit id="addNewPartner" value="Request add new partner"/>
	    	</div>
	    	
	    </s:form>
	    
    </div> 
 
</body>
</html>    