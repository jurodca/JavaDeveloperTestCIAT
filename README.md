DESCRIPTION
- This project is focused on responding to the Java Developer test for CIAT 
- The project is done in Java 1.8.0_131 and Apache Tomcat 8.5.13 and Eclipse Neon.3 Release (4.6.3)

INSTRUCTIONS

- Clone the project
- Change the path in the config.properties located in src/main/webapp/database (You can choose Windows based users, Mac OSX based users or create a new one)
- Package the project using Maven
- Deploy in your Tomcat Server

jrodriguezc@gmail.com
