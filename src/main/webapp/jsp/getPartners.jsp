<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%> 
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<title>Jurodca - Java Developer Test - CIAT</title>
	<sb:head/>
</head>
<body >
	<div class="container">
	   <s:actionerror theme="bootstrap"/>
       <s:actionmessage theme="bootstrap"/>
       <s:fielderror theme="bootstrap"/>
	     
	    <h2>List of Institutions</h2>
	    
	    <table class="table table-striped">
		    <tr >
		        <th><s:text name="Headquarter"/></th>
		        <th><s:text name="Acronym"/></th>
		        <th><s:text name="Name"/></th>
		        <th><s:text name="Location"/></th>
		    </tr>
		    <s:iterator value="partnerList" status="status">
		    
		    	 <tr >
		            <td><s:property value="headquarter"/></td>
		            <td><s:property value="acronym"/></td>
		            <td><s:property value="name"/></td>
		            <td><s:property value="location"/></td>
        		 </tr>   
		    
		    </s:iterator>
	    </table>
	    
	    <s:form id="frmReturn" method="post" action="enterApp"> 
	    	<s:submit id="idReturn" value="Add Institution" cssClass="btn btn-default"/>
	    </s:form>
	    
    </div> 
 
</body>
</html>    