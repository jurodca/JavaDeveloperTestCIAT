<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%> 
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<title>Jurodca - Java Developer Test - CIAT</title>
	<script>


		$(function() {
			
			 $(document).on('click', 'input[type="button"]', function(event) {
			    let id = this.id;
				
			    if(id == "btnYes"){
			    	$('#lblHdq').show();
			    	$('#idHdq').show();
			    	$('#idBranch').val('Yes')
			    	$('#idHdq').val('No');
			    	$("#btnYes").addClass("btn btn-primary");
			    	$("#btnNo").removeClass("btn btn-primary");
			    	
			    }else{
			    	if(id == "btnNo"){
			    		$('#idHdq').val('Yes');
			    		$('#lblHdq').hide();
				    	$('#idHdq').hide();
			    		$('#idBranch').val('No');
			    		$("#btnNo").addClass("btn btn-primary");
			    		$("#btnYes").removeClass("btn btn-primary");
			    		
			    	}
			    }
			    
			    
			  });
			    
		});
	</script>
	<sb:head/>
</head>
<body >
	<div class="container">
	     
	    <div class="jumbotron">
	    	<div class="container">
			  <h2>Request a new institution or branch</h2>
			  <p>
		    	Please enter the institution as "Partner", the legal entity.<br>
		    	The name of the institution should be in its official language. (e.g. for CIAT: Centro Internacional de Agricultura Tropical). 
		     </p>
	      
  			</div>
		</div> 
	   
	   <s:actionerror theme="bootstrap"/>
       <s:actionmessage theme="bootstrap"/>
       <s:fielderror theme="bootstrap"/>
	    
	    <s:form id="frmPartners" method="post" action="addPartner" theme="bootstrap" cssClass="form-vertical well">
				
			<label>Is this institution a branch:</label>
			<input type="button" id="btnYes"  value="Yes" class="btn btn-default"/>
			<input type="button" id="btnNo"  value="No" class="btn btn-default"/> 
	 
	    	
	    	<hr />
	    		
			    <s:hidden id="idBranch" name="isBranch"/>
			    <s:label id="lblHdq" value="Select institution headquarter"/>
			    <s:select id="idHdq" 
			    		  headerKey="-1" headerValue="Select an option"
			    		  list="headquartersList"
			    		  name="headquarters"
			    		  listKey="acronym"
			    		   listValue="name"
			     />	
	    	
	    	
	    			  
			    <s:label value="Acronym"/>
			    <s:textfield name="acronym"/>
	    		
			    <s:textfield name="name" elementCssClass="col-md-8" label="Name" labelCssClass="col-md-4"/>
	    		
	    		<s:label value="Type"/>
	    		<s:select  name="type" 
		         value="%{'Academic Institution'}"
		         list="{'Academic Institution','Donor','Non-Governmental Organization','Research Institution'}" />
	    	
	    		<s:label value="Country"/>
	    		<s:select  name="country" 
		         value="%{'Colombia'}"
		         list="{'Argentina','Colombia','Canada','M�xico','United States','Per�'}" />
		         
		        <s:label value="City"/>
	    		<s:textfield name="city"/>
	    		
	    		<s:label value="If you know the partner website please paste the link above"/>
	    		<s:textfield name="website"/>
	    		
	    		<s:submit id="addNewPartner" value="Request add new partner" cssClass="btn btn-default"/>
	    	
	    	
	    </s:form>
	    
    </div> 
 
</body>
</html>    