<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="/struts-tags" prefix="s"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<title>Jurodca - Java Developer Test - CIAT</title>	
</head>
<body >
	<s:if test="hasActionErrors()">
	   <div>
	      <s:actionerror/>
	   </div>
	</s:if>
     
    <h2>Hi, welcome to the Java Developer Test for CIAT</h2>
     
    <s:form id="enter" method="post" action="enterApp">
    	<s:submit id="submit" value="Enter Website"/>
    </s:form> 
 
</body>
</html>    