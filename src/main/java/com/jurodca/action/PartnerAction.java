package com.jurodca.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.jurodca.entities.Partner;
import com.jurodca.services.PartnerServiceImpl;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;


/**
 * A class to manage the actions of the app
 * @author jurodca
 *
 */
public class PartnerAction extends ActionSupport implements Preparable { 

	private static final long serialVersionUID = 1L;
	
	//objects to manage data
	private Partner partner;
	private PartnerServiceImpl partnerService = new PartnerServiceImpl();
	private List<Partner> partnerList;
	private List<Partner> headquartersList = new ArrayList<Partner>();
	
	//fields of the form
	private String isBranch="";
	private String headquarter="";
	private String acronym="";
	private String name="";
	private String type="";
	private String country="";
	private String city="";
	private String website="";
	private String idHeadquarter="";
	
	String msg=null;
	
	
	@Override
	@Action(value="enterApp", results={
			@Result(name=SUCCESS,location="/jsp/mainPartners.jsp")
			,@Result(name=INPUT, location="/jsp/mainPartners.jsp")		
		})
	public String execute() {  
        return SUCCESS;
    }
	
	
	/**
	 * addPartner action. Adds a new partner into the database
	 * if the addition is succesfully, then redirects to getPartners.jsp
	 * otherwise redirects to mainPartners.jsp with the message errors
	 * @author jurodca
	 * @return
	 */
	@Action( value="addPartner", results= {
		     @Result(name=SUCCESS, type="dispatcher", location="/jsp/getPartners.jsp")
		     ,@Result(name=INPUT, location="/jsp/mainPartners.jsp")
	})
	public String addPartner() {
		
		try{
			
			this.partner = new Partner();
			partner.setIsBranch(isBranch);
			
			if(isBranch.equals("Yes")){
				headquarter = "No";
				partner.setHeadquarter(headquarter);
			}else{
				headquarter = "Yes";
				partner.setHeadquarter(headquarter);
			}
			
			String directory = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/database");
			
			partner.setAcronym(acronym);
			partner.setName(name);
			partner.setCountry(country);
			partner.setType(type);
			partner.setCity(city);
			partner.setWebsite(website);
			
			partnerService.addPartner(partner,directory);
			addActionMessage("Partner added successfully!");
			partnerList = partnerService.getPartners(directory);	
			
			//clean all data
			this.cleanAllData();
			
			return SUCCESS;
			
		}catch(Exception e){
			e.printStackTrace();
			msg = "Error: "+e.getMessage();
			addActionError(msg);
			return ERROR;
		}
		
	}
	
	
	/**
	 * clean all the data
	 * @author jurodca
	 */
	public void cleanAllData(){
		this.isBranch = "";
		this.headquarter = "";
		this.acronym = "";
		this.name = "";
		this.type = "";
		this.country = "";
		this.city = "";
		this.website = "";
	}
	
	/**
	 * prepare the data to list in the mainPartners.jsp
	 * @author jurodca
	 */
	@Override
	public void prepare() throws Exception {
		
		String directory = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/database");
	    
		List<Partner> headquartesTmp = null;
		headquartesTmp = partnerService.getHeadquartersPartners(directory);
			
		if(headquartesTmp!=null){
			headquartersList = headquartesTmp;
		}else{
			headquartersList = new ArrayList<>();
		}

	}
	
	
	/**
	 * custom validations
	 * @author jurodca
	 */
	@Override
	public void validate(){
		
		String directory = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/database");
		
		if(name.length()>0){
			if(name.split("\\s+|\n").length>10){
				addFieldError("name", "The name field must be less than 10 words.");
			}
		}	
		
		if(website.length()>0){
			if(!website.startsWith("http://")){
				if(!website.startsWith("https://")){
					addFieldError("website", "The url must start with http:// or https://.");
				}
				
			}
		}
		
		//validate the data with the database (File)
		List<Partner> partnerListTmp = partnerService.getPartners(directory);
		
		if(isBranch.equals("No"))
		
			for(Partner partner : partnerListTmp){
			
				if(partner.getName().equalsIgnoreCase(name)){
					addFieldError("name", "There is a partner with the same name, please use a different one.");
				}
				
				if(partner.getAcronym().equalsIgnoreCase(acronym)){
					addFieldError("acronym", "There is a partner with the same acronym, please use a different one.");
				}
			
		}else{
			
			for(Partner partner : partnerListTmp){
				if(partner.getCountry().equalsIgnoreCase(country)){
					if(partner.getCity().equalsIgnoreCase(city)){
						addFieldError("city", "There is a partner located in the same city and country, please use a different one.");
					}
				}
			}
			
		}
		
		
		
		
	}
	
	/****************************
	 * Getters and Setters
	 * *************************/
	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}



	public List<Partner> getPartnerList() {
		return partnerList;
	}



	public void setPartnerList(List<Partner> partnerList) {
		this.partnerList = partnerList;
	}

	
	public String getIsBranch() {
		return isBranch;
	}


	public void setIsBranch(String isBranch) {
		this.isBranch = isBranch;
	}

	
	public String getHeadquarter() {
		return headquarter;
	}

	public void setHeadquarter(String headquarter) {
		this.headquarter = headquarter;
	}


	public String getAcronym() {
		return acronym;
	}

	@RequiredStringValidator(message = "The acronym field is required!")
	@StringLengthFieldValidator(minLength="1", maxLength="10", message="The acronym field must be less than 10 characters")
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}


	public String getName() {
		return name;
	}

	@RequiredStringValidator(message = "The name field is required!")
	public void setName(String name) {
		this.name = name;
	}


	public String getType() {
		return type;
	}

	@RequiredStringValidator(message = "The type field is required!")
	public void setType(String type) {
		this.type = type;
	}


	public String getCountry() {
		return country;
	}

	@RequiredStringValidator(message = "The country field is required!")
	public void setCountry(String country) {
		this.country = country;
	}


	public String getCity() {
		return city;
	}

	@RequiredStringValidator(message = "The city field is required!")
	public void setCity(String city) {
		this.city = city;
	}


	public String getWebsite() {
		return website;
	}


	public void setWebsite(String website) {
		this.website = website;
	}

	public String getIdHeadquarter() {
		return idHeadquarter;
	}

	public void setIdHeadquarter(String idHeadquarter) {
		this.idHeadquarter = idHeadquarter;
	}

	public List<Partner> getHeadquartersList() {
		return headquartersList;
	}


	public void setHeadquartersList(List<Partner> headquartersList) {
		this.headquartersList = headquartersList;
	}
	
	
}
