package com.jurodca.entities;

/**
 * Entity class Partner. 
 * @author jurodca
 *
 */
public class Partner {
	
	private String isBranch;
	private String headquarter;
	private String acronym;
	private String name;
	private String type;
	private String country;
	private String city;
	private String website;
	private String urlFile;
	private String location;
	
	public String getIsBranch() {
		return isBranch;
	}
	public void setIsBranch(String isBranch) {
		this.isBranch = isBranch;
	}
	public String getHeadquarter() {
		return headquarter;
	}
	public void setHeadquarter(String headquarter) {
		this.headquarter = headquarter;
	}
	public String getAcronym() {
		return acronym;
	}
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	
	public String getUrlFile() {
		return urlFile;
	}
	public void setUrlFile(String urlFile) {
		this.urlFile = urlFile;
	}

	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String toString(){
		return this.isBranch+","
			   +this.headquarter+","
			   +this.acronym+","
			   +this.name+","
			   +this.type+","
			   +this.country+","
			   +this.city+","
			   +(this.website.length()>0?this.website:"EMPTY")+"\r\n";
	}

}
