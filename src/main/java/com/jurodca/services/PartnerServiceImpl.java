package com.jurodca.services;

import java.util.List;

import com.jurodca.entities.Partner;
import com.jurodca.interfaces.services.PartnersService;
import com.jurodca.persistence.PartnerDAOImpl;

public class PartnerServiceImpl implements PartnersService {
	
	private PartnerDAOImpl partnerDao = new PartnerDAOImpl();

	/**
	 * create a new partner into the database
	 * @author jurodca
	 * @param partner - an object with the partner attributes
	 * @param urlDatabase - the url where database is located
	 * @throws Exception
	 */
	@Override
	public void addPartner(Partner partner,String urlDatabase) throws Exception {
		
		//all the validations here
		partnerDao.addPartner(partner,urlDatabase);

	}

	/**
	 * get the differentes partners from the database
	 * @author jurodca
	 * @param urlDatabase - the url where database is located
	 * @throws Exception
	 */
	@Override
	public List<Partner> getPartners(String urlDatabase) {
	
		return partnerDao.getPartners(urlDatabase);
	}

	/**
	 * get the differentes headquarters partners from the database
	 * @author jurodca
	 * @param urlDatabase - the url where database is located
	 * @throws Exception
	 */
	@Override
	public List<Partner> getHeadquartersPartners(String urlDatabase) {
		// TODO Auto-generated method stub
		return partnerDao.getHeadquartersPartners(urlDatabase);
	}

}
