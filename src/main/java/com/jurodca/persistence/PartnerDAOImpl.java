package com.jurodca.persistence;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.jurodca.entities.Partner;
import com.jurodca.interfaces.dao.PartnerDAO;
import com.jurodca.utils.Utilities;

public class PartnerDAOImpl implements PartnerDAO {

	/**
	 * create a new partner into the database
	 * @author jurodca
	 * @param partner - an object with the partner attributes
	 * @param urlDatabase - the url where database is located
	 * @throws Exception
	 */
	@Override
	public void addPartner(Partner partner,String urlDatabase) throws Exception {
		
		FileWriter database = null;
		PrintWriter pw = null;
		try{
			
			String directory = Utilities.getFileLocation(urlDatabase);
			//String directory = "/tmp";
			
			String filename = directory+"/database.txt";
			
			String record = partner.toString();
			
			database = new FileWriter(filename, true);
			pw = new PrintWriter(database);
			pw.print(record);
				
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
			try {
		       if (null != database)
		         database.close();
		    } catch (Exception e2) {
		        e2.printStackTrace();
		    }
			
		}

	}

	/**
	 * get the differentes partners from the database
	 * @author jurodca
	 * @param urlDatabase - the url where database is located
	 * @throws Exception
	 */
	@Override
	public List<Partner> getPartners(String urlDatabase) {
		List<Partner> listPartner = new ArrayList<>();
		Partner partnerTmp = null;
		
		File database = null;
	    FileReader fr = null;
	    BufferedReader br = null;
		
		try{
			
			String directory = Utilities.getFileLocation(urlDatabase);
			database = new File (directory+"/database.txt");
	        fr = new FileReader (database);
	        br = new BufferedReader(fr);

	      
	         String line;
	         while((line=br.readLine())!=null){
	        	 String record = line; 
	        	 //break the string in tokens using (,) like separator
	        	 if(record!=null){
	        		 partnerTmp = Utilities.getPartner(record);
	        		 
	        		 if(partnerTmp!=null){
	        			 listPartner.add(partnerTmp);
	        		 }	 
	        	 }
	        	 
	         }
	           
			 
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
	        
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	      }
		
		return listPartner;
		
	}

	
	/**
	 * get the differentes headquarters partners from the database
	 * @author jurodca
	 * @param urlDatabase - the url where database is located
	 * @throws Exception
	 */
	@Override
	public List<Partner> getHeadquartersPartners(String urlDatabase) {
		List<Partner> listPartner = new ArrayList<>();
		Partner partnerTmp = null;
		
		File database = null;
	    FileReader fr = null;
	    BufferedReader br = null;
		
		try{
			String directory = Utilities.getFileLocation(urlDatabase);
			database = new File (directory+"/database.txt");
	        fr = new FileReader (database);
	        br = new BufferedReader(fr);

	      
	         String line;
	         while((line=br.readLine())!=null){
	        	 String record = line; 
	        	 //break the string in tokens using (,) like separator
	        	 if(record!=null){
	        		 partnerTmp = Utilities.getPartner(record);
	        		 
	        		 if(partnerTmp!=null && partnerTmp.getIsBranch().equals("No")){
	        			 listPartner.add(partnerTmp);
	        		 }	 
	        	 }
	        	 
	         }
	           
			 
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
	        
	         try{                    
	            if( null != fr ){   
	               fr.close();     
	            }                  
	         }catch (Exception e2){ 
	            e2.printStackTrace();
	         }
	      }
		
		return listPartner;
	}

}
