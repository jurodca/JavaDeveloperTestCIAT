package com.jurodca.interfaces.dao;

import java.util.List;

import com.jurodca.entities.Partner;

public interface PartnerDAO {
	
	/**
	 * create a new partner into the database
	 * @author jurodca
	 * @param partner - an object with the partner attributes
	 * @param urlDatabase - the url where database is located
	 * @throws Exception
	 */
	public void addPartner(Partner partner,String urlDatabase) throws Exception;
	
	/**
	 * get the differentes partners from the database
	 * @author jurodca
	 * @param urlDatabase - the url where database is located
	 * @throws Exception
	 */
	public List<Partner> getPartners(String urlDatabase); 
	
	/**
	 * get the differentes headquarters partners from the database
	 * @author jurodca
	 * @param urlDatabase - the url where database is located
	 * @throws Exception
	 */
	public List<Partner> getHeadquartersPartners(String urlDatabase);

}
