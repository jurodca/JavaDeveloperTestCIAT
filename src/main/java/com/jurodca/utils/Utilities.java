package com.jurodca.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.jurodca.entities.Partner;

/**
 * Basic utilities class
 * 
 * @author julian
 *
 */
public class Utilities {
	
	
	/**
	 * get the url where a database is located
	 * @author jurodca
	 * @param urlDatabase - url where file properties is located
	 * @return
	 * @throws IOException
	 */
	public static String getFileLocation(String urlDatabase) throws IOException{
		
		String directory="";
		
		try {
			   
			
			 Properties prop = new Properties();
			 prop.load(new FileInputStream(urlDatabase+"/config.properties"));
			 directory = prop.getProperty("database.path");
			   
			       
			    
		} catch (FileNotFoundException e) {
			   System.out.println("Error, El archivo no exite");
		} catch (IOException e) {
			   System.out.println("Error, No se puede leer el archivo");
		}
		
		return directory;	
	}
	
	
	/**
	 * return a Partner object from a String data array.
	 * @author jurodca
	 * @param data - array with partner's attributes
	 * @return
	 */
	public static Partner getPartner(String data){
		Partner partnerTmp = new Partner();
		
		String dataArray[] = data.split(",");
		
		partnerTmp.setIsBranch(dataArray[0]);
		partnerTmp.setHeadquarter(dataArray[1]);
		partnerTmp.setAcronym(dataArray[2]);
		partnerTmp.setName(dataArray[3]);
		partnerTmp.setType(dataArray[4]);
		partnerTmp.setCountry(dataArray[5]);
		partnerTmp.setCity(dataArray[6]);
		partnerTmp.setWebsite(dataArray[7]==null?"":dataArray[7]);
		partnerTmp.setLocation(dataArray[6]+","+dataArray[5]);
		
		return partnerTmp;
		
		
	}

}
